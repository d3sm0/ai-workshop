class: center, middle, inverse
name: AI: Basic principles
# {{name}}
### simone@ai-academy.com
.footnote[.left[Roma 20.01.2017]]
---
class: inverse
name: Structure
## {{name}}

### 1. Definitions
### 2. Problem setting
### 3. Problem types
### 4. Shortcomings
### 5. The hard part...
???

Alla fine di quest ora imparerete come si organizza un problema di AI e di cosa avete bisogno per affrontarlo.

---

class:center, middle, inverse
name: Learning
## {{name}}

Apprendere e' l'atto di **acquisire** nuove **capacita** e migliorarne di estitenti,
attraverso la **sintesi** di nuove **informazioni**.

<!-- ???
Cosa vuol dire apprendere?
 -->

- Pensate per un attimo a come impariamo noi a camminare, guardiamo i nostri genitori farlo,
cadiamo, un sacco di volte, cercando di camminare e poi eventualmente ce la facciamo.

---

class: center, middle, inverse
name: Machine Learning
## {{name}}

"_Il Machine Learning e' una branca di CS che da al computer l'abilita di **apprendere** senza essere programmato in modo **esplicito**_"

(Arthur Samuel, 1959).

---
class: inverse
## Data
.center[![retail_data](img/retail_data.png)]

Una **feature** e' una caratteristica associata a ciascun oggetto, o azione d'interesse.

---

class: inverse
exclude: True

## Feature
.right-column[
![beer](img/beer_match.jpg)
]

???
Sono un produttore di birra e voglio predire il consumo di birra nei prossimi mesi, quali feature posso valutare?

---

class: inverse
exclude: True

## Feature
.left-column[
### Temperatura
### Evento
### Area geografica
]
.right-column[
![beer](img/beer_match.jpg)
]

---

class: inverse
exclude: True

## You try...
.left-column[
### Features?
]
.right-column[
![cat](img/cat.jpg)

L'insieme delle features, determinano una classe, che chiameremo **label**.
]

---

class: inverse
exclude: True

## You try...
.left-column[
### Features?
### Label?
]
.right-column[
![hp](img/hp.jpg)
]

---
class: center, middle, inverse
## General functions

Una funzione generica che al crescere dei dati ricevuti, impara a descrivere il fenomeno osservato.

???
Ripensate al azienda che produce birra
---
name: general
class: center, middle, inverse
???
Ripensate al azienda che produce birra
---

class: inverse
## General Functions

.left-column[
### Supervised learning
]
.right-column[
![fruits](img/clustering.jpg)

La macchina apprende attraverso migliaia di input gia pre-classificati, ovvero con **label**.
]
???
Un esempio di classificazione?
---

class: inverse
exclude: True

## General Functions
.left-column[
### Supervised learning
### Unsupervised learning
]
.right-column[
![cats](img/cats.jpg)

La macchina apprende la relazione tra le features delle migliaia di input somministrati.
]

???
Esempio di unsupervised learning
---
class:inverse
## General Functions
.left-column[
### Supervised learning
### Unsupervised learning
### Recommendation
]
.right-column[
![movie](img/movie.png)

La macchina apprende attraveso la relazione gruppi di persone simili.
]


???
Classificazione tante features, con label
  obiettivo -> metodo automatico che predice la categoria
Clustering abbiamo tante features, senza label
  obiettivo -> relazione tra le feature
recommendation, abbiamo tante informazioni parziali
  obiettivo -> una funzione che ci dica quanto un oggetto puo interessare

---

  class: center, middle, inverse
  name: First principle
  exclude: True

  ## {{name}}
  Le macchine fanno solo cio che gli insegni, nulla di piu.

  ???
  - Ad esempio, se paradossalmente se insegnamo ad un umano a riconoscere diversi cani, sara in grado di differenziare
  i cani dai gatti, ed eventualmente assegnarli ad una classe.
  - La macchina no, e' molto molto stupida, ad esempio non sarebbe capace di riconoscere un bicchiere rotto o intero.
  Oppure piu nel pratico, non e' detto che sia capace di riconoscere due foto di gatti, una proviente dal vostro telefono
  e una presa da internet. Questo perche appunto, impara solo quello che voi volete che impari.

---
class: inverse
exclude: True

## Classification
![fruits](img/clustering.jpg)

???
Supponiamo di avere una nuova app che classifica le foto dei nostri utenti,
fino ad oggi i nostri utenti le hanno classificate a mano, ora voglionom che l'app le classifichi automaticamnete
---
class: inverse
## Classification
.left-column[
### Goal
]
.right-column[
- Rilasciare una nuova feature che classifichi automaticamente le foto dei nostri utenti al fine di **migliorare** l'UX.
]
---

class: inverse
exclude: True

## Classification
.left-column[
### Goal
### Function
]
.right-column[
- Rilasciare una nuova feature che classifichi automaticamente le foto dei nostri utenti al fine di **migliorare** l'UX.
- Funzione che date alcune caratteristiche delle foto, le assegni ad una categoria.
]
---

class: inverse
exclude: True

## Classification
.left-column[
### Goal
### Function
### Data
]
.right-column[
- Rilasciare una nuova feature che classifichi automaticamente le foto dei nostri utenti al fine di **migliorare** l'UX.
- Funzione che date alcune caratteristiche delle foto, le assegni ad una categoria.
- 1K -> 10K foto di frutta
]
---

class: inverse
exclude: True

## Classification
.left-column[
### Goal
### Function
### Data
### Tools
]
.right-column[
#### Linear Regression
![linear_regression](img/linearRegression.png)
]
---

class:inverse

## Classification
.left-column[
### Goal
### Function
### Data
### Tools
]
.right-column[
#### Support Vector Machine
![svm](img/svm.png)
]

???
Linear regression -> esprime appunto una relazione che minimizza la distanza tra la linea e i punti nel piano

SVM -> confine decisionale che separa le classi in modo tale da avere il confine il piu lontano possibile dal dato piu vicino

Multi layer perceptron -> per quello che interessa a noi,
abbiamo degli input al primo stadio, che attivano i nostri neuorni, il segnale viene di solito interpreatto
da un secondo strato di neuroni, che viene a suoa volta interpretato dagli axon

Domande?
---

class: inverse
exclude: True

## Clustering
![retail_data](img/retail_data.png)

???
Supponiamo di avere una nuovo e-commerce e vogliamo segmentare gli utenti secondo le loro abitudini di acquisto
Non abbiamo un target di predizione, bensi stiamo cercando le relazioni tra i nostri dati

---

class: inverse
exclude: True

## Clustering
.left-column[
### Goal
]
.right-column[
- Identificare 2 o piu gruppi di persone con abitutidini di acquisto simili al fine di migliorare l'advertising.
]

---

class: inverse
exclude: True

## Clustering
.left-column[
### Goal
### Function
]
.right-column[
- Identificare 2 o piu gruppi di persone con abitutidini di acquisto simili al fine di migliorare l'advertising.
- Trovare una funzione che divida i gruppi di dati "simili".
]

---

class: inverse
exclude: True

## Clustering
.left-column[
### Goal
### Function
### Data
]
.right-column[
- Identificare 2 o piu gruppi di persone con abitutidini di acquisto simili al fine di migliorare l'advertising.
- Trovare una funzione che divida i gruppi di dati "simili".
- Dati acquisto, oggetti acquistati, review, descrizioni etc...
]

---

class:inverse

## Clustering
.left-column[
### Goal
### Function
### Data
### Tools
]
.right-column[
#### K-means
![kmeans_2d](img/kmeans.png)
]
???
- metto 3 segnalini nel grafico assegno i dati piu vicini a ciascun segnalino,
per ciuascun dato - segnalino faccio la media, ed avvicino il segnalino di meta.
---

name: recomendation
class: inverse
exclude: True

## Recommendation
.left-column[
![gelato](img/gelato.png)
![sushi](img/sushi.jpg)
![pasta](img/pasta.jpg)
]

.right-column[

![reccomender](img/reccomender.jpg)]


???
App che raccoglie i dati dell'insulina dei malati di diabete, e vogliamo suggerire
un alimentazione corretta in base ai dati che abbiamo e le loro preferenze

---

class:inverse
exclude: True

## Recommendation
.left-column[
### Goal
]
.right-column[
- Suggerire un alimento adatto ai nostri utenti, al fine di migliorare la loro salute ed il valore finale percepito dell'applicazione.
]

???
Come misuriamo il fatto che sia adatto?
- sia stato scelto in qualche modo
- suggerito da persone simili

Insomma che dati ci servono?
- ci servono sia i dati dei pazienti
- delle loro pfereferenze
- e delle tipologie di cibo

---

class:inverse
exclude: True

## Recommendation
.left-column[
### Goal
### Function
]
.right-column[
- Suggerire un alimento adatto ai nostri utenti, al fine di migliorare la loro salute ed il valore finale percepito dell'applicazione.
- Funzione che che associ utenti, cibo e preferenze.
]
---

class: inverse
exclude: True

## Recommendation
.left-column[
### Goal
### Function
### Data
]
.right-column[
![friends](img/friends01.jpeg)
]

---

class: inverse

## Recommendation
.left-column[
### Goal
### Function
### Data
### Tools
]
.right-column[
#### Content based filtering
Suggerire nuovi cibi, *simili* rispetto a cio che ha l'utente ha mangiato in passato.
#### Collaborative filtering
Suggerire nuovi cibi, rispetto a cio che utenti *simili* hanno mangiato in passato.
]
???
In matematica, due oggetti sono simili tra di loro se sono diversi, e se la loro distanza e' molt piccola
---
class:inverse, middle, center

## Shortcomings

Limiti di del Machine Learning
Le performance dell'algortimo non miglioraro all'aumentare del tempo di apprendimento.

---
## Shortcomings
.left-column[
### Generalization
### Overfitting
### Similar inputs
]
.right-column[
Non _generalizza_ rispetto a nuovi inputs.
Il modello e' troppo dipendente dai dati che ha imparato.
Le caratteristiche di ciascun input nella foto, sono troppo simili per essere riconosciute
]

---


class:inverse
exclude: true

## Shortcomings
.left-column[
### Generalization
]
.right-column[
Non _generalizza_ rispetto a nuovi inputs

![generalization](img/generalization-issue.png)
]
???
Generalizzare
Estendere la propria esperienza a qualcosa che non hai visto ma e' simile rispetto a cose che hai gia visto.

---

class:inverse
exclude: true

## Shortcomings

.left-column[
### Generalization
### Overfitting
]

.right-column[
Il modello e' troppo dipendente dai dati che ha imparato.

![overfitting](img/overfitting.png)
]
???
precisione 100% l'ultima
precisione 10% generalizza meglio
Come possiamo vedere il nostro algoritmo non e' in grado di predire una chiara appartenenza
dei nuovi sample

---
class:inverse
exclude: true

## Shortcomings
.left-column[
### Generalization
### Overfitting
### Similar inputs
]
.right-column[
Le caratteristiche di ciascun input nella foto, sono troppo simili per essere riconosciute

![cats](img/cats.jpg)
]
---
class:inverse
exclude: true

## Shortcomings
.left-column[
### Generalization
### Overfitting
### Similar inputs
### Learning rate
]
.right-column[
Le performance dell'algortimo non miglioraro all'aumentare del tempo di apprendimento.

![learning](img/learning.png)
]
---
class: inverse, center, middle
## Hello Deep Learning!

.lego_special[![lego](img/dl.jpg)]

???
Utilizziamo diversi strati di neuroni per estrarre le caratteristiche principali nei nostri dati.
Passiamo da un livello di concretezza molto completo, ovvero colori dei pixels, fimo ad
un astrazione di concetto molto alta, ovvero cane

---

class:inverse
exclude: true

## Deep Learning
.left-column[
### Raw data
### Abstraction
]
.right-column[
![cat](img/cat.jpg)

Quali sono le features in questa foto?
]
---

class:inverse
## Deep Learning
.left-column[
### Raw data
### Abstraction
### Performance
]
.right-column[
![chart](img/dl_chart.jpg)
]


???
- lavorare con raw data, video, immagini, testo,
- gestire una mole di dati molto molto grande
- costurire livelli di astrazione piu complessi, da immagini, a classi e frasi
- aggiungi macchina
---

exclude: true
class:inverse

## Object recognition

Vogliamo che i nostri clienti possano prendere la roba e andarsene senze dover passare
per la cassa.

![lego_shop](img/lego_shop.jpg)

---

class:inverse
exclude: true

## Object recognition
.left-column[
### Goal
]
.right-column[
- Permettere alle persone di acquistare senza passare per la cassa.
]
---
class:inverse
## Object recognition
.left-column[
### Goal
### Function
]
.right-column[
- Permettere alle persone di acquistare senza passare per la cassa.
- Identicare l'oggetto preso dalla persona, verificare che esca dal locale con il nuovo acquisto.
]

---

class:inverse
exclude: true

## Object recognition
.left-column[
### Goal
### Function
### Data
]
.right-column[
- Permettere alle persone di acquistare senza passare per la cassa.
- Identicare l'oggetto preso dalla persona, verificare che esca dal locale con il nuovo acquisto.
- Molte ore di filmato del locale, e un app sul telefono per l'identicazione dell'account cliente
]

---

class:inverse
exclude: true

## Object recognition
.left-column[
### Goal
### Function
### Data
### Tool
]
.right-column[
![googl_net](img/googlenet1.png)
]
---
class:inverse
## Object recognition
.left-column[
### Goal
### Function
### Data
### Tool
]
.right-column[
![googl_net](img/googlenet2.png)
]

---

class:inverse
exclude: true

## Recap

### Problem setting
### Classification: Supervised learning
### Clustering: Unsupervised learning
### Recommendation
### Machine Learning shortcomings

---
class: inverse center middle
## Keep In Mind

Machine learning e' una caratteristica del prodotto e **non il** prodotto.

???
Esplicitiamo il business need, applichiamo il nostro framework Target, Task, Data, Tool, e iniziamo a a sperimentare

---

class:center middle inverse
exclude: True

## Q&A

---

class:center middle inverse

## AMA

---

class:inverse
exclude: True

## Your turn
Dove vorreste applicare il Machine Learning?

.left-column[
### Goal
### Features
### Function
]
