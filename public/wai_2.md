class: center, middle, inverse
name: AI: Basic principles
# {{name}}
### simone@ai-academy.com
.footnote[.left[Roma 20.01.2017]]
---
class: inverse
name: Structure
## {{name}}

### 1. Definitions
### 2. Problem setting
### 3. Problem types
### 4. Shortcomings
### 5. The hard part...
???

Alla fine di quest ora imparerete come si organizza un problema di AI e di cosa avete bisogno per affrontarlo.

---

class:center, middle, inverse
name: Learning
## {{name}}

Apprendere e' l'atto di **acquisire** nuove **capacita** e migliorarne di estitenti,
attraverso la **sintesi** di nuove **informazioni**.

---

class: center, middle, inverse
name: Machine Learning

## {{name}}

"_Il Machine Learning e' una branca di CS che da al computer l'abilita di **apprendere** senza essere programmato in modo **esplicito**_"

(Arthur Samuel, 1959).

---

class: inverse
## Data
.center[![retail_data](img/retail_data.png)]

Una **feature** e' una caratteristica associata a ciascun oggetto, o azione d'interesse.

---

class: center, middle, inverse

## General functions

Una funzione generica che al crescere dei dati ricevuti, impara a descrivere il fenomeno osservato.

???
Ripensate al azienda che produce birra

---

class: inverse
## General Functions

.left-column[
### Supervised learning
]
.right-column[
![fruits](img/clustering.jpg)

La macchina apprende attraverso migliaia di input gia pre-classificati, ovvero con **label**.
]
???
Un esempio di classificazione?

---

class: inverse

## General Functions
.left-column[
### Supervised learning
### Unsupervised learning
]
.right-column[
![cats](img/cats.jpg)

La macchina apprende la relazione tra le features delle migliaia di input somministrati.
]

???
Esempio di unsupervised learning

---
class:inverse
## General Functions
.left-column[
### Supervised learning
### Unsupervised learning
### Recommendation
]
.right-column[
![movie](img/movie.png)

La macchina apprende attraveso la relazione gruppi di persone simili.
]


???
Classificazione tante features, con label
  obiettivo -> metodo automatico che predice la categoria
Clustering abbiamo tante features, senza label
  obiettivo -> relazione tra le feature
recommendation, abbiamo tante informazioni parziali
  obiettivo -> una funzione che ci dica quanto un oggetto puo interessare

---
class: inverse

## Classification
![fruits](img/clustering.jpg)

???
Supponiamo di avere una nuova app che classifica le foto dei nostri utenti,
fino ad oggi i nostri utenti le hanno classificate a mano, ora voglionom che l'app le classifichi automaticamnete

---

class:inverse

## Classification
.left-column[
### Goal
### Tools
]
.right-column[
#### Support Vector Machine
![svm](img/svm.png)
]

---

class: inverse

## Clustering
![retail_data](img/retail_data.png)

---

class:inverse

## Clustering
.left-column[
### Goal
### Tools
]
.right-column[
#### K-means
![kmeans_2d](img/kmeans.png)
]

---

name: recomendation
class: inverse

## Recommendation
.left-column[
![gelato](img/gelato.png)
![sushi](img/sushi.jpg)
![pasta](img/pasta.jpg)
]

.right-column[

![reccomender](img/reccomender.jpg)]


???
App che raccoglie i dati dell'insulina dei malati di diabete, e vogliamo suggerire
un alimentazione corretta in base ai dati che abbiamo e le loro preferenze

---

class: inverse

## Recommendation
.left-column[
### Goal
### Tools
]
.right-column[
#### Content based filtering
Suggerire nuovi cibi, *simili* rispetto a cio che ha l'utente ha mangiato in passato.
#### Collaborative filtering
Suggerire nuovi cibi, rispetto a cio che utenti *simili* hanno mangiato in passato.
]


---
class:inverse, middle, center

## Shortcomings

Limiti di del Machine Learning
Le performance dell'algortimo non miglioraro all'aumentare del tempo di apprendimento.

---

class: inverse

## Shortcomings
.left-column[
### Generalization
### Overfitting
### Similar inputs
### Learning rate
]

.right-column[

- Non _generalizza_ rispetto a nuovi inputs.
- Il modello e' troppo dipendente dai dati che ha imparato.
- Le caratteristiche di ciascun input nella foto, sono troppo simili per essere riconosciute.
- Le performance dell'algortimo non miglioraro all'aumentare del tempo di apprendimento.
]

---

class: inverse, center, middle

## Hello Deep Learning!

.lego_special[![lego](img/dl.jpg)]

---

class: inverse

## Deep Learning

.right-column[
![chart](img/dl_chart.jpg)
]

---

class:inverse

## Object recognition
.right-column[
![googl_net](img/googlenet1.png)
]

---

class:inverse
## Object recognition

.right-column[
![googl_net](img/googlenet2.png)
]

---

class: inverse center middle
## Keep In Mind

Machine learning e' una caratteristica del prodotto e **non il** prodotto.

???
Esplicitiamo il business need, applichiamo il nostro framework Target, Task, Data, Tool, e iniziamo a a sperimentare

---
class:center middle inverse

## AMA

---

class:inverse
exclude: True

## Your turn
Dove vorreste applicare il Machine Learning?

.left-column[
### Goal
### Features
### Function
]
