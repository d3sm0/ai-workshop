
<!-- class: inverse
## Problem setting
.left-column[
### Goal
]
.right-column[
- Cosa voglio migliorare del mio prodotto?
]
---
class:inverse
## Problem setting
.left-column[
### Goal
### Task
]
.right-column[
- Cosa voglio migliorare del mio prodotto?
- Quali sono gli step che la macchina deve apprendere?
]

---
class:inverse
## Problem setting
.left-column[
### Goal
### Task
### Data
]
.right-column[
- Cosa voglio migliorare del mio prodotto?
- Quali sono gli step che la macchina deve apprendere?
- Di quali dati ho bisogno?
]
---
class:inverse
## Problem setting
.left-column[
### Goal
### Task
### Data
### Tools
]
.right-column[
- Cosa voglio migliorare del mio prodotto?
- Quali sono gli step che la macchina deve apprendere?
- Di quali dati ho bisogno?
- Quali strumenti posso usare?
]

---
class: inverse
##  Problem setting
.left-column[
### Goal
### Task
### Data
### Tools
]
.right-column[
![driving_school](img/driving_school.jpg)
]
???
Fare la slide ad icone
Imparare a guidare -> capire come si sterza -> ascoltare l'istruttore -> usare la macchina

Domande? -->


---
